<?php
class Miembro extends CI_Model
{
  function __construct()
  {
    parent::__construct();
  }
  function insertar($datosNuevoMiembro){
    return $this->db->insert('miembros',$datosNuevoMiembro);
  }
  function obtenerTodos(){
      $listadoMiembros=$this->db->get('miembros');
      if ($listadoMiembros->num_rows()>0) {
        return $listadoMiembros->result();
      } else {
        return false;
      }
  }
  function borrar($id_mie){
    $this->db->where('id_mie',$id_mie);
    return $this->db->delete('miembros');
  }
}
?>
