<?php
class Rutina extends CI_Model
{
  function __construct()
  {
    parent::__construct();
  }
  function insertar($datosNuevaRutina){
    return $this->db->insert('rutinas',$datosNuevaRutina);
  }
  function obtenerTodos(){
      $listadoRutinas=$this->db->get('rutinas');
      if ($listadoRutinas->num_rows()>0) {
        return $listadoRutinas->result();
      } else {
        return false;
      }
  }
  function borrar($id_rut){
    $this->db->where('id_rut',$id_rut);
    // Aqui se debe poner la tabla de donde borrar con el where anterior
    return $this->db->delete('rutinas');
  }
}
?>
