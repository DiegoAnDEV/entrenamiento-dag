<?php
class Entrenador extends CI_Model
{
  function __construct()
  {
    parent::__construct();
  }
  function insertar($datosNuevoEntrenador){
    return $this->db->insert('entrenadores',$datosNuevoEntrenador);
  }
  function obtenerTodos(){
      $listadoEntrenadores=$this->db->get('entrenadores');
      if ($listadoEntrenadores->num_rows()>0) {
        return $listadoEntrenadores->result();
      } else {
        return false;
      }
  }
  function borrar($id_ent){
    $this->db->where('id_ent',$id_ent);
    return $this->db->delete('entrenadores');
  }
}
?>
