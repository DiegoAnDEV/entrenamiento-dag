<?php
  class Entrenadores extends CI_Controller
  {
    function __construct()
    {
      parent::__construct();
      $this->load->model('Entrenador');
    }
    public function servicios()
  	{
  		$this->load->view('header');
  		$this->load->view('/entrenadores/servicios');
  		$this->load->view('footer');
  	}
    public function nuevo()
  	{
  		$this->load->view('header');
  		$this->load->view('/entrenadores/nuevo');
  		$this->load->view('footer');
  	}
    public function listado()
  	{
      $data['entrenadores']=$this->Entrenador->obtenerTodos();
  		$this->load->view('header');
  		$this->load->view('/entrenadores/listado',$data);
  		$this->load->view('footer');
  	}
    public function insertar(){
      $datosNuevoEntrenador = array(
        'nombre_ent' =>$this->input->post('nombre_ent'),
        'apellido_ent' =>$this->input->post('apellido_ent'),
        'especialidad_ent' =>$this->input->post('especialidad_ent'),
        'edad_ent' =>$this->input->post('edad_ent')
    );
      if ($this->Entrenador->insertar($datosNuevoEntrenador)) {
        redirect('entrenadores/listado');
      } else {
        echo "<h1>ERROR AL INSERTAR</h1>";
      }
    }

    public function eliminar($id_ent){
    if ($this->Entrenador->borrar($id_ent)) {
      redirect('entrenadores/listado');
    } else {
      echo "<h1>ERROR AL BORRAR</h1>";
    }
  }
  }
?>
