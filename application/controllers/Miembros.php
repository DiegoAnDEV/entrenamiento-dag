<?php
  class Miembros extends CI_Controller
  {
    function __construct()
    {
      parent::__construct();
      $this->load->model('Miembro');
    }
    public function nuevo()
  	{
  		$this->load->view('header');
  		$this->load->view('/miembros/nuevo');
  		$this->load->view('footer');
  	}
    public function listado()
  	{
      $data['miembros']=$this->Miembro->obtenerTodos();
  		$this->load->view('header');
  		$this->load->view('/miembros/listado',$data);
  		$this->load->view('footer');
  	}
    public function insertar(){
      $datosNuevoMiembro = array(
        'nombre_mie' =>$this->input->post('nombre_mie'),
        'apellido_mie' =>$this->input->post('apellido_mie'),
        'cedula_mie' =>$this->input->post('cedula_mie'),
        'edad_mie' =>$this->input->post('edad_mie')
    );
      if ($this->Miembro->insertar($datosNuevoMiembro)) {
        redirect('miembros/listado');
      } else {
        echo "<h1>ERROR AL INSERTAR</h1>";
      }
    }

    public function eliminar($id_mie){
    if ($this->Miembro->borrar($id_mie)) {
      redirect('miembros/listado');
    } else {
      echo "<h1>ERROR AL BORRAR</h1>";
    }
  }
  }
?>
