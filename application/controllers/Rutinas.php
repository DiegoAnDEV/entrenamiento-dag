<?php
  class Rutinas extends CI_Controller
  {
    function __construct()
    {
      parent::__construct();
      $this->load->model('Rutina');
    }
    public function nuevo()
  	{
  		$this->load->view('header');
  		$this->load->view('/rutinas/nuevo');
  		$this->load->view('footer');
  	}
    public function listado()
  	{
      $data['rutinas']=$this->Rutina->obtenerTodos();
  		$this->load->view('header');
  		$this->load->view('/rutinas/listado',$data);
  		$this->load->view('footer');
  	}
    public function insertar(){
      $datosNuevaRutina = array(
        'desc_rut' =>$this->input->post('desc_rut'),
        'horario_rut' =>$this->input->post('horario_rut'),
        'especialidad_rut' =>$this->input->post('especialidad_rut')
    );
      if ($this->Rutina->insertar($datosNuevaRutina)) {
        redirect('rutinas/listado');
      } else {
        echo "<h1>ERROR AL INSERTAR</h1>";
      }
    }

    public function eliminar($id_rut){
    if ($this->Rutina->borrar($id_rut)) {
      redirect('rutinas/listado');
    } else {
      echo "<h1>ERROR AL BORRAR</h1>";
    }
  }
  }
?>
