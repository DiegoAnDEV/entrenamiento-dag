<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <!-- jquery-->
    <script src="https://code.jquery.com/jquery-3.7.0.min.js" integrity="sha256-2Pmvv0kuTBOenSvLm6bvfBSSHrUJ+3A7x6P5Ebd07/g=" crossorigin="anonymous"></script>
    <!-- Boostrap 3 -->
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@3.3.7/dist/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <!-- Optional theme -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@3.3.7/dist/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
    <!-- Latest compiled and minified JavaScript -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@3.3.7/dist/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
    <title>Entrenamientos DAG</title>
  </head>
    <body>
<!-- Inicio del cuerpo -->
<!-- AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA -->
<style>
  header[role="banner"] {
  #logo-main {
    display: block;
    margin: 20px auto;
  }
  }

  #navbar-primary.navbar-default {
  background: transparent;
  border: none;
  .navbar-nav {
    width: 100%;
    text-align: center;
    > li {
      display: inline-block;
      float: none;
      > a {
        padding-left: 30px;
        padding-right: 30px;
        }
    }
  }
  }
</style>

<header role="banner">
  <img id="logo-main" src="<?php echo base_url();?>/assets/imgs/logotipo.png" width="150px" alt="Logo">
<nav id="navbar-primary" class="navbar navbar-default" role="navigation">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-primary-collapse">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
    </div>
    <div class="collapse navbar-collapse" id="navbar-primary-collapse">
      <ul class="nav navbar-nav">
        <li><a href="<?php echo site_url();?>">Inicio</a></li>
        <li><a href="<?php echo site_url('entrenadores/servicios');?>">Servicios</a></li>
        <li class="dropdown">
        <a class="dropdown-toggle" data-toggle="dropdown" href="#">Entrenadores
        <span class="caret"></span></a>
        <ul class="dropdown-menu">
          <li><a href="<?php echo site_url('/entrenadores/nuevo');?>">Nuevo Entrenador</a></li>
          <li><a href="<?php echo site_url('/entrenadores/listado');?>">Listado de Entrenadores</a></li>
        </ul>
        </li>

        <li class="dropdown">
        <a class="dropdown-toggle" data-toggle="dropdown" href="#">Rutinas
        <span class="caret"></span></a>
        <ul class="dropdown-menu">
          <li><a href="<?php echo site_url('/rutinas/nuevo');?>">Nueva Rutina</a></li>
          <li><a href="<?php echo site_url('/rutinas/listado');?>">Listado de Rutinas</a></li>
        </ul>
        </li>

        <li class="dropdown">
        <a class="dropdown-toggle" data-toggle="dropdown" href="#">Miembros
        <span class="caret"></span></a>
        <ul class="dropdown-menu">
          <li><a href="<?php echo site_url('/miembros/nuevo');?>">Nuevo Miembro</a></li>
          <li><a href="<?php echo site_url('/miembros/listado');?>">Listado de Miembros</a></li>
        </ul>
        </li>
      </ul>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>
</header><!-- header role="banner" -->
