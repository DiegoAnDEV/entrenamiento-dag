<h1>Ingrese un Nuevo Miembro</h1>
<hr>
<form class="" action="<?php echo site_url();?>/Miembros/insertar" method="post">
<!-- Aqui se define el tipo de método para el input, este es de tipo post -->
    <div class="row">
      <div class="col-md-6">
        <img src="<?php echo base_url('/assets/imgs/logotipo.png');?>" alt="" height="500px" width="100%">
      </div>

      <div class="col-md-4">
          <!-- Nombre -->
          <label for="">Nombre:</label>
          <br>
          <input type="text"
          placeholder="Ingrese el nombre del miembro"
          class="form-control"
          name="nombre_mie" value=""
          id="nombre_mie">
          <!-- Apellido -->
          <label for="">Apellido:</label>
          <br>
          <input type="text"
          placeholder="Ingrese el apellido del miembro"
          class="form-control"
          name="apellido_mie" value=""
          id="apellido_mie">
          <!-- Especialidad -->
          <label for="">Cédula:</label>
          <br>
          <input type="text"
          placeholder="Ingrese la cédula del nuevo miembro"
          class="form-control"
          name="cedula_mie" value=""
          id="cedula_mie">
          <!-- Edad -->
          <label for="">Edad:</label>
          <br>
          <input type="number"
          placeholder="Ingrese la edad del miembro"
          class="form-control"
          name="edad_mie" value=""
          id="edad_mie">
          <!-- Botones -->
          <br>
          <button type="submit" name="button"
          class="btn btn-primary">
            Guardar
          </button>
          &nbsp;
          <a href="<?php echo site_url();?>/miembros/listado"class="btn btn-danger">Cancelar</a>
      </div>
    </div>
</form>
