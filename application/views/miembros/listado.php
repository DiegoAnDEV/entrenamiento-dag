<div class="row">
  <div class="col-md-8">
    <h1 class="text-center">Listado de Miembros</h1>
  </div>
  <div class="col-md-4">
    <a href="<?php echo site_url();?>/miembros/nuevo"><i class="glyphicon glyphicon-plus"></i> Nuevo Miembro</a>
  </div>
</div>
<hr>
<?php if ($miembros): ?>
  <table class="table table-bordered table-hover">
    <thead class="thead-dark">
      <tr>
        <th>ID</th>
        <th>Nombre</th>
        <th>Apellido</th>
        <th>Cedula</th>
        <th>Edad</th>
        <th>Acción</th>
      </tr>
    </thead>
    <tbody>
      <?php foreach ($miembros as $filatemporal): ?>
        <tr>
          <td>
            <?php echo $filatemporal->id_mie?>
          </td>
          <td>
            <?php echo $filatemporal->nombre_mie?>
          </td>
          <td>
            <?php echo $filatemporal->apellido_mie?>
          </td>
          <td>
            <?php echo $filatemporal->cedula_mie?>
          </td>
          <td>
            <?php echo $filatemporal->edad_mie?>
          </td>
          <td class="text-center">
            <a href="<?php echo site_url();?>/Miembros/eliminar/<?php echo $filatemporal->id_mie?>" title="Eliminar Miembro" onclick="return confirm('Estás seguro');" style="color:red">
              <i class="glyphicon glyphicon-trash"></i>
            </a>
          </td>
        </tr>
      <?php endforeach; ?>
    </tbody>
  </table>
<?php
  // print_r($instructores); un print_r para imprimir todo el valor del array y comprobar que si pasó la información
 ?>
<?php else: ?>
<h1>No hay datos</h1>
<?php endif; ?>
