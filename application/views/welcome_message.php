<style>
	.jumbotron {
	position: relative;
	overflow: hidden;
	background-color:black;
	}
	.jumbotron video {
	position: absolute;
	z-index: 1;
	top: 0;
	width:100%;
	height:100%;
	/*  object-fit is not supported on IE  */
	object-fit: cover;
	opacity:0.5;
	}
	.jumbotron .container {
	z-index: 2;
	position: relative;
	}
</style>

<script type="text/javascript">
	function deferVideo() {
		//defer html5 video loading
	$("video source").each(function() {
		var sourceFile = $(this).attr("data-src");
		$(this).attr("src", sourceFile);
		var video = this.parentElement;
		video.load();
		// uncomment if video is not autoplay
		//video.play();
	});
	}
	window.onload = deferVideo;
</script>

<div class="row">
	<div class="jumbotron jumbotron-fluid">
	<video autoplay loop>
			<source src="" data-src="<?php echo base_url();?>/assets/vids/videoa480.mp4" type="video/mp4">
			<source src="" data-src="<?php echo base_url();?>/assets/vids/videoa480.webm " type="video/webm">
			<source src="" data-src='https://storage.cloudconvert.com/tasks/580d96e6-7b4d-4527-afa4-b0039ad4c36a/videoa480.webm?AWSAccessKeyId=cloudconvert-production&Expires=1685715547&Signature=K3jN%2FBH57BIVjs2QR3qKS8qIvM4%3D&response-content-disposition=inline%3B%20filename%3D%22videoa480.webm%22&response-content-type=video%2Fwebm'
			type="video/webm">
	</video>

		<div class="container text-white" style="color:white">
			<h1 class="display-4">Hora de ponerse en forma!</h1>
			<p class="lead">Hoy es el día en el que cambiarás tus hábitos y mejorarás tu físico, ¿Qué esperas?.</p>
			<hr class="my-4">
			<p>Con nosotros podrás cumplir tus metas sin sentirte juzgado por tu apariencia.</p>
		</div>
		<!-- /.container -->
	</div>
</div>

<div class="row">
	<div class="col-md-6 text-center">
		<img src="<?php echo base_url();?>/assets/imgs/ini1.jpg" alt="" width="100%">
	</div>
	<div class="col-md-6 text-center" style="margin-top:5em;margin-bottom:auto;">
		<h2>Comienza tu entrenamiento</h2>
		<hr>
		<p>Con nosotros puedes empezar de forma inmediata tu entrenamiento y empezar a llevar un estilo de vida más sano.</p>
	</div>
</div>

<div class="row">
	<div class="col-md-6 text-center" style="margin-top:5em;margin-bottom:auto;">
		<h2>Hacer ejercicio de forma agradable</h2>
		<hr>
		<p>Nos aseguramos de que disfrutes de tu ejercicio, para evitar que se convierta en una tarea molesta,
			nuestras rutinas y entrenadores se aseguran de brindarte el mejor apoyo en tu camino.</p>
	</div>
	<div class="col-md-6 text-center">
		<img src="<?php echo base_url();?>/assets/imgs/ini2.png" alt="" width="100%" height="500px">
	</div>
</div>
