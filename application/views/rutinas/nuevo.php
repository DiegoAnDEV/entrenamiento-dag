<h1>Ingrese una Nueva Rutina</h1>
<hr>
<form class="" action="<?php echo site_url();?>/Rutinas/insertar" method="post">
<!-- Aqui se define el tipo de método para el input, este es de tipo post -->
    <div class="row">
      <div class="col-md-6">
        <img src="<?php echo base_url('/assets/imgs/logotipo.png');?>" alt="" height="500px" width="100%">
      </div>

      <div class="col-md-4">
          <!-- Nombre -->
          <label for="">Descripción:</label>
          <br>
          <input type="text"
          placeholder="Ingrese una descripción breve de la rutina"
          class="form-control"
          name="desc_rut" value=""
          id="desc_rut">
          <!-- Apellido -->
          <label for="">Horario:</label>
          <br>
          <input type="text"
          placeholder="Ingrese el horario de la rutina"
          class="form-control"
          name="horario_rut" value=""
          id="horario_rut">
          <!-- Especialidad -->
          <label for="">Especialidad:</label>
          <br>
          <input type="text"
          placeholder="Ingrese la especialidad de esta rutina"
          class="form-control"
          name="especialidad_rut" value=""
          id="especialidad_rut">
          <!-- Botones -->
          <br>
          <button type="submit" name="button"
          class="btn btn-primary">
            Guardar
          </button>
          &nbsp;
          <a href="<?php echo site_url();?>/rutinas/listado"class="btn btn-danger">Cancelar</a>
      </div>
    </div>
</form>
