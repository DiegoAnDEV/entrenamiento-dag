<div class="row">
  <div class="col-md-8">
    <h1 class="text-center">Listado de Rutinas</h1>
  </div>
  <div class="col-md-4">
    <a href="<?php echo site_url();?>/rutinas/nuevo"><i class="glyphicon glyphicon-plus"></i> Nueva Rutina</a>
  </div>
</div>
<hr>
<?php if ($rutinas): ?>
  <table class="table table-bordered table-hover">
    <thead class="thead-dark">
      <tr>
        <th>ID</th>
        <th>Descripción</th>
        <th>Horario</th>
        <th>Especialidad</th>
        <th>Acción</th>
      </tr>
    </thead>
    <tbody>
      <?php foreach ($rutinas as $filatemporal): ?>
        <tr>
          <td>
            <?php echo $filatemporal->id_rut?>
          </td>
          <td>
            <?php echo $filatemporal->desc_rut?>
          </td>
          <td>
            <?php echo $filatemporal->horario_rut?>
          </td>
          <td>
            <?php echo $filatemporal->especialidad_rut?>
          </td>
          <td class="text-center">
            <a href="<?php echo site_url();?>/Rutinas/eliminar/<?php echo $filatemporal->id_rut?>" title="Eliminar Rutina" onclick="return confirm('Estás seguro');" style="color:red">
              <i class="glyphicon glyphicon-trash"></i>
            </a>
          </td>
        </tr>
      <?php endforeach; ?>
    </tbody>
  </table>
<?php
  // print_r($instructores); un print_r para imprimir todo el valor del array y comprobar que si pasó la información
 ?>
<?php else: ?>
<h1>No hay datos</h1>
<?php endif; ?>
