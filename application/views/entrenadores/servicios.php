<div class="row">
	<div class="col-md-6 text-center">
		<img src="<?php echo base_url();?>/assets/imgs/servicios1.png" alt="" width="100%">
	</div>
	<div class="col-md-6 text-center" style="margin-top:5em;margin-bottom:auto;">
		<h2>Entrenadores Personales</h2>
		<hr>
		<p>Ofrecemos un gran rango de entrenadores especializados en distintas áreas.
			Siempre podrás elegir el que necesites en función de tu meta.
		</p>
	</div>
</div>

<div class="row">
	<div class="col-md-6 text-center" style="margin-top:5em;margin-bottom:auto;">
		<h2>Planificación alimenticia</h2>
		<hr>
		<p>El primer paso hacia una vida saludable es una alimentación saludable.
			Nosotros te ayudamos a obtener la mejor dieta de acuerdo a tus necesidades.
		</p>
	</div>
	<div class="col-md-6 text-center">
		<img src="<?php echo base_url();?>/assets/imgs/servicios2.png" alt="" width="100%">
	</div>
</div>

<div class="row">
	<div class="col-md-6 text-center">
		<img src="<?php echo base_url();?>/assets/imgs/servicios3.png" alt="" width="100%">
	</div>
	<div class="col-md-6 text-center" style="margin-top:5em;margin-bottom:auto;">
		<h2>Entrenamiento Intensivo</h2>
		<hr>
		<p>Desde calistenia y aeróbicos hasta artes marciales.
			Nuestros entrenadores están más que capacitados para entrenarte de forma exigente y fructífera en la disciplina que desees.
		</p>
	</div>
</div>
