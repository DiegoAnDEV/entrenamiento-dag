<div class="row">
  <div class="col-md-8">
    <h1 class="text-center">Listado de Entrenadores</h1>
  </div>
  <div class="col-md-4">
    <a href="<?php echo site_url();?>/entrenadores/nuevo"><i class="glyphicon glyphicon-plus"></i> Nuevo Entrenador</a>
  </div>
</div>
<hr>
<?php if ($entrenadores): ?>
  <table class="table table-bordered table-hover">
    <thead class="thead-dark">
      <tr>
        <th>ID</th>
        <th>Nombre</th>
        <th>Apellido</th>
        <th>Especialidad</th>
        <th>Edad</th>
        <th>Acción</th>
      </tr>
    </thead>
    <tbody>
      <?php foreach ($entrenadores as $filatemporal): ?>
        <tr>
          <td>
            <?php echo $filatemporal->id_ent?>
          </td>
          <td>
            <?php echo $filatemporal->nombre_ent?>
          </td>
          <td>
            <?php echo $filatemporal->apellido_ent?>
          </td>
          <td>
            <?php echo $filatemporal->especialidad_ent?>
          </td>
          <td>
            <?php echo $filatemporal->edad_ent?>
          </td>
          <td class="text-center">
            <a href="<?php echo site_url();?>/Entrenadores/eliminar/<?php echo $filatemporal->id_ent?>" title="Eliminar Entrenador" onclick="return confirm('Estás seguro');" style="color:red">
              <i class="glyphicon glyphicon-trash"></i>
            </a>
          </td>
        </tr>
      <?php endforeach; ?>
    </tbody>
  </table>
<?php
  // print_r($instructores); un print_r para imprimir todo el valor del array y comprobar que si pasó la información
 ?>
<?php else: ?>
<h1>No hay datos</h1>
<?php endif; ?>
