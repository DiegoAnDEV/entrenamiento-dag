<h1>Ingrese un Nuevo Entrenador</h1>
<hr>
<form class="" action="<?php echo site_url();?>/Entrenadores/insertar" method="post">
<!-- Aqui se define el tipo de método para el input, este es de tipo post -->
    <div class="row">
      <div class="col-md-6">
        <img src="<?php echo base_url('/assets/imgs/logotipo.png');?>" alt="" height="500px" width="100%">
      </div>

      <div class="col-md-4">
          <!-- Nombre -->
          <label for="">Nombre:</label>
          <br>
          <input type="text"
          placeholder="Ingrese el nombre del entrenador"
          class="form-control"
          name="nombre_ent" value=""
          id="nombre_ent">
          <!-- Apellido -->
          <label for="">Apellido:</label>
          <br>
          <input type="text"
          placeholder="Ingrese el apellido del entrenador"
          class="form-control"
          name="apellido_ent" value=""
          id="apellido_ent">
          <!-- Especialidad -->
          <label for="">Especialidad:</label>
          <br>
          <input type="text"
          placeholder="Ingrese la especialidad del entrenador"
          class="form-control"
          name="especialidad_ent" value=""
          id="especialidad_ent">
          <!-- Edad -->
          <label for="">Edad:</label>
          <br>
          <input type="number"
          placeholder="Ingrese la edad del entrenador"
          class="form-control"
          name="edad_ent" value=""
          id="edad_ent">
          <!-- Botones -->
          <br>
          <button type="submit" name="button"
          class="btn btn-primary">
            Guardar
          </button>
          &nbsp;
          <a href="<?php echo site_url();?>/entrenadores/listado"class="btn btn-danger">Cancelar</a>
      </div>
    </div>
</form>
