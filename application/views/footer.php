<div class="row" style="background-color:#97f556">
  <div class="col-md-4 text-center">
    <h3><u>Misión</u></h3>
    <p>Convertirnos en la cadena de gimnasios
    <br> y servicios de entrenamiento privado
    <br> por excelencia a nivel nacional</p>
  </div>
  <div class="col-md-4 text-center">
    <h3><u>Visión</u></h3>
    <p>Mejorar el estilo de vida
    <br>de muchas personas a través
    <br>del ejercicio y buen acompañamiento.
    </p>
  </div>
  <div class="col-md-4 text-center">
    <h3><u>Contáctenos</u></h3>
    <h5><u>Teléfonos</u></h5>
    <p>09878986756
      <br>03647584931
    </p>
  </div>
</div>
<div class="row" style="background-color:black;color:white">
  <p class="text-left col-md-6">Todos los derechos reservados - Latacunga, Ecuador</p>
  <p class="text-right col-md-6">Desarrollado por: DiegoAnDEV - 2023</p>
</div>

<!-- AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA -->
<!-- Fin del cuerpo -->
  </body>
</html>
